<?php
/**
 * Plugin Name: Atondes Wordpress Plugin
 * Version: 1.0.0
 * Plugin URI: https://gitlab.com/atondes
 * Author: Atondes Collective
 * Author URI: https://gitlab.com/atondes
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: atondes-wordpress-plugin
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Atondes Collective
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files.
require_once 'includes/class-atondes-wordpress-plugin.php';
require_once 'includes/class-atondes-wordpress-plugin-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-atondes-wordpress-plugin-admin-api.php';
require_once 'includes/lib/class-atondes-wordpress-plugin-post-type.php';
require_once 'includes/lib/class-atondes-wordpress-plugin-taxonomy.php';

/**
 * Returns the main instance of Atondes_Wordpress_Plugin to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object Atondes_Wordpress_Plugin
 */
function atondes_wordpress_plugin() {
	$instance = Atondes_Wordpress_Plugin::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = Atondes_Wordpress_Plugin_Settings::instance( $instance );
	}

	return $instance;
}

atondes_wordpress_plugin();

add_shortcode('comment_gallery', 'comment_gallery');

function comment_gallery ($atts, $content = null)
{
	global $pagenow;
	if (( $pagenow == 'post.php' ) ) {
		return;
	}	
	
	global $post;

	if (!$post)
	{
		return;
	}

	global $wpdb;

	$query =
		"SELECT p1.id, p1.post_title, pm1.meta_key, pm1.meta_value, c.comment_content, p2.ID as attachment_id, p2.guid, pm2.meta_value as attachment_metadata, pm3.meta_value as event_link
		FROM wp_posts as p1
		LEFT JOIN wp_postmeta as pm1 on p1.ID = pm1.post_id 
		LEFT JOIN wp_comments as c on c.comment_post_ID = p1.ID
		LEFT JOIN wp_commentmeta as cm on c.comment_ID = cm.comment_id
		LEFT JOIN wp_posts as p2 on p2.ID = cm.meta_value
		LEFT JOIN wp_postmeta as pm2 on pm2.post_id = p2.id
		LEFT JOIN wp_postmeta as pm3 on pm3.post_id = p1.id
		WHERE p1.id = %d
		AND pm1.meta_key = 'ar'
		AND c.comment_approved = 1
		AND (p2.guid != null OR p2.guid != '')
		AND pm2.meta_key = '_wp_attachment_metadata'
		AND pm3.meta_key = 'event_link'";

	$result = $wpdb->get_results($wpdb->prepare($query, $post->ID));

	if ($result)
	{
		$content .= 
		"
		<style>
			div.mini_gallery {
				margin: 5px;
				border: 1px solid #ccc;
				float: left;
				width: 170px;
			}
		
			div.mini_gallery:hover {
				border: 1px solid #777;
			}
		
			div.mini_gallery img {
				width: 100%;
				height: auto;
			}
		
			div.desc {
				padding: 15px;
				text-align: center;
			}
		
			div.findme {
				display: flex;
				flex-wrap: wrap;
			}

			div.full_width {
				width: 100%;
				height: 4em;
			}
		</style>
		";
		
		$content .= "<div class='findme'>";
		$first = false;

		foreach($result as $line)
		{
			if ($first == false)
			{
				$content .= "<div class='full_width'> <a href='$line->meta_value'> Event Link </a></div>";
				$first = true;
			}

			$metadata= wp_get_attachment_metadata( $line->attachment_id );
			$path = dirname(wp_get_original_image_url( $line->attachment_id ));

			$content .= "<div class='mini_gallery'>";

			if ($metadata && $metadata["sizes"])
			{
				// Small pictures dont get resized versions at all
				if ($metadata["sizes"]["large"])
				{
					$content .= "<a href='" . $path . "/" . $metadata["sizes"]["large"]["file"] . "'>";
				}
				else
				{
					$content .= "<a href='$line->guid'>";
				}

				if ($metadata["sizes"]["medium"])
				{
					$content .= "<img src='" . $path . "/" . $metadata["sizes"]["medium"]["file"] . "' alt='' width='155' height='140'>";
				}
				else
				{
					$content .= "<img src='" . $line->guid . "' alt='' width='155' height='140'>";
				}
			}
			else
			{
				$content .= "
					<a href='$line->guid'>
					<img src='$line->guid' alt='' width='155' height='140'>";
			}

			$content .= "</a></div>";
		}
	}
	else
	{
		$content .= "This image gallery is still empty. Share your favorite picture below.";
	}

	$content .= "</div>";

	return $content;
}

function get_user_media_for_post($data)
{
	global $wpdb;

	$query = "SELECT p1.id, p1.post_title, pm1.meta_key, pm1.meta_value, c.comment_content, p2.ID as attachment_id, p2.guid, pm2.meta_value as attachment_metadata, pm3.meta_value as event_link
	FROM wp_posts as p1
	LEFT JOIN wp_postmeta as pm1 on p1.ID = pm1.post_id 
	LEFT JOIN wp_comments as c on c.comment_post_ID = p1.ID
	LEFT JOIN wp_commentmeta as cm on c.comment_ID = cm.comment_id
	LEFT JOIN wp_posts as p2 on p2.ID = cm.meta_value
	LEFT JOIN wp_postmeta as pm2 on pm2.post_id = p2.id
	LEFT JOIN wp_postmeta as pm3 on pm3.post_id = p1.id
	WHERE pm1.meta_key = 'ar'
	AND c.comment_approved = 1
	AND (p2.guid != null OR p2.guid != '')
	AND pm2.meta_key = '_wp_attachment_metadata'
	AND pm3.meta_key = 'event_link'
	AND pm1.meta_value = %s";

	$result = $wpdb->get_results($wpdb->prepare($query, $data->get_param( 'ar_key' )));

	$rootObject = new RootObject();
	$rootObject->postList = array();

	if ($result)
	{
		$post = new Post();
		array_push($rootObject->postList, $post);
		$post->commentList = array();

		foreach($result as $line)
		{
			$post->post_title = $line->post_title;
			$post->meta_value = $line->meta_value;
			$post->event_link = $line->event_link;

			$comment = new Comment();
			array_push($post->commentList, $comment);

			$comment->text = $line->comment_content;
			$comment->original_image = $line->guid;

			$metadata= wp_get_attachment_metadata( $line->attachment_id );
			$path = dirname(wp_get_original_image_url( $line->attachment_id ));

			if ($metadata)
			{
				// Small pictures dont get resized versions at all
				if ($metadata["sizes"])
				{
					if ($metadata["sizes"]["medium"])
					{
						$comment->preview_image = $path . "/" . $metadata["sizes"]["medium"]["file"];
					}

					if ($metadata["sizes"]["large"])
					{
						$comment->large_image = $path . "/" . $metadata["sizes"]["large"]["file"];
					}
				}
			}
		}
	}

	return $rootObject;
} 

class RootObject
{
	public $postList;
}

class Post {
	public $meta_value;
	public $post_title;
	public $commentList;
	public $event_link;
}

class Comment {
	public $text;
	public $original_image;
	public $preview_image;
	public $large_image;
}

add_action( 'rest_api_init', function () 
	{
		register_rest_route( 'atondes-wordpress-plugin/v1', '/getUserMediaForPost', 
			array(
				'methods' => 'GET',
				'callback' => 'get_user_media_for_post',
			)
		);

		register_rest_route( 'testplugin/v1', '/getUserMediaForPost', 
			array(
				'methods' => 'GET',
				'callback' => 'get_user_media_for_post',
			)
		);
	}
);