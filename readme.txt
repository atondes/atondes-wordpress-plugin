=== Atondes Wordpress Plugin ===
Contributors: Atondes Collective
Tags: wordpress, plugin, template
License: MIT
License URI: https://opensource.org/licenses/MIT

The Atondes Wordpress Plugin exposes verified comments with images through a REST api to be consumed by the Atondes AR App

== Installation ==

Currently, the installation is done by copying the project folder into the wordpress's plugin folder.

== Changelog ==

= 1.0 =
* 2020-07-03
* Initial release